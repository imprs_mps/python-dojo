#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Exceptions unit test example

When testing for exceptions the exception is the desired behaviour.
This means that the test shall only fail if no exception was raised.

There is a very simple helper available in pytest to do this in a
save and short way

@author: hoelken
"""

import pytest
from src.dojo import Hal9000


def test_open_pod_bay_doors() -> None:
    """
    This method shall raise an exception. Test is passed if this is the case
    """
    hal = Hal9000(user='Dave')
    with pytest.raises(RuntimeError) as info:
        hal.open_pod_bay_doors()
    # You can even test for the exception message, if you want to
    assert "I'm sorry, Dave. I'm afraid I can't do that" == info.value.args[0]
