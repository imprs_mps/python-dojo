"""
Tests for decorator pattern examples
"""
from src.dojo.design_patterns.decorator import Stringifyer, Filter


def test_raw_behaviour():
    """
    Test the base object behaviour without decorators
    """
    strify = Stringifyer()
    assert 'test' == strify.stringify('test')
    assert '1' == strify.stringify(1)


def test_one_decorator():
    """
    Test the base object behaviour with a single decorator
    """
    strify = Stringifyer()
    strify = Filter(strify, 'es', replacement='?')
    assert 't?t' == strify.stringify('test')


def test_multiple_decorators():
    """
    Test the base object behaviour with multiple decorators
    """
    strify = Stringifyer()
    strify = Filter(strify, 'one', replacement='?')
    strify = Filter(strify, '1', replacement='!')
    assert 'test' == strify.stringify('test')
    assert '!' == strify.stringify(1)
    assert '3.!4!' == strify.stringify(3.141)
    assert 'I am root!!!!!?eleven' == strify.stringify('I am root!!!11oneeleven')
