#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module that holds example classes for testing

@author: hoelken
"""

import time


class Hal9000:
    """
    # HAL 9000
    example class

    This class provides simple methods to showcase some testing scenarios
    """

    # 72 hrs in seconds to avoid magic numbers.
    SEVENTY_TWO_HRS = 259200

    def __init__(self, user=None):
        #: The name of the user
        self.user = user
        self.failed_module = 'AE35'

    def greet(self) -> str:
        """
        Sends greetings to the current user.

        :return: the greeting string.
        """
        if self.user is None:
            return 'Hello world!'
        return 'Affirmative, %s. I read you.' % self.user

    def self_test(self) -> dict:
        """
        checks for failed modules

        :return: the name of the module (if any) and the time left (seconds) until failure will be 100%
        """
        return {self.failed_module: Hal9000.SEVENTY_TWO_HRS}

    def random(self, factor: int = 1):
        """
        :return: a pseudo random integer generated from the NS timestamp and the users name
        """
        return len(self.user) * time.time_ns() * factor

    def open_pod_bay_doors(self) -> None:
        """
        In case you are stuck outside the vessel, open the pod bay doors...
        """
        raise RuntimeError("I'm sorry, %s. I'm afraid I can't do that" % self.user)


class DeepThought:
    """
    # Deep Thought

    Deep Thought is a computer that was created by a pan-dimensional, hyper-intelligent
    species of beings (whose three-dimensional protrusions into our universe are ordinary
    white mice) to come up with the Answer to The Ultimate Question of Life, the Universe,
    and Everything. See
    [Wikipedia](https://en.wikipedia.org/wiki/List_of_The_Hitchhiker%27s_Guide_to_the_Galaxy_characters#Deep_Thought)
    """

    #: Seven and a half million years in seconds
    SEVEN_HALF_MIO_YEARS = 2.366771e14

    @staticmethod
    def compute_answer() -> int:
        """
        This will take a while...

        :return: the answer to live, universe and everything
        """
        time.sleep(DeepThought.SEVEN_HALF_MIO_YEARS)
        return 42
