# Python Dojo

### Contents
- [Requirements](#requirements)
- [Getting started](#getting_started)
- [Contributing](#contributing)

## Requirements
This code is tested under linux (ubuntu) but should be platform independent. 
We run on `python >= 3.8`.

## Getting started
### What to find here
#### Idea
The idea behind this repo is to have a go-to place where one can find examples, best practices 
and learning resources to make yourself familiar with python unit testing. 

Consider this repo as a self-support group where anyone can contribute their insights.

Also, the setup, structure and `.gitlab-ci` can be used as a **template** for your own python repository. 

#### Structure
##### Unit Testing
All **example tests** can be found in the `tests/examples` folder. They all make use of 
code documentation and try to show minimal examples for certain typical testing 
situations. The aim is that our examples are self-explanatory.

If 'modules/classes under test' are referred, you will find them in the `src/dojo/testing_examples` folder.
You can also go there to find **Documentation** best practices. The results are visible 
on the [GitLab pages](https://imprs_mps.pages.gwdg.de/python-dojo/doc/dojo/testing_examples/). 

##### Design Patterns
All **design_pattern** examples can be found in the `src/dojo/design_patterns` folder. They provide minimal 
example implementations of software design patterns. The examples also have 
[Gitlab Pages](https://imprs_mps.pages.gwdg.de/python-dojo/doc/dojo/design_patterns/index.html) where they are explained. 
Their tests & usage examples can be found in the `tests/design_patterns` folder.

##### Notebooks
for some case we have also `Jupyter` notebooks. You can access (read) them in 
[gitlab](https://gitlab.gwdg.de/imprs_mps/python-dojo/-/tree/main/notebooks) directly. 
However, for interactive use you might want to load this dojo in your jupiter-notebook installation.   

##### Further Read
**Learning resources** (Links) and **How-To guides** can be found in our 
[wiki](https://gitlab.gwdg.de/imprs_mps/python-dojo/-/wikis/home). 

#### Disclaimer
**Remember** Nothing of this is set in stone. All examples shall give you an idea on how to solve 
your problem. Also, we try to stick to the `PEP-8` guid, but - as you know - real life isn't always as in the 
books so please feel free to deviate as needed.

### Checkout
To create a local copy of this repository, checkout using `git`:

```bash
$ cd /path/to/repos/
$ git clone git@gitlab.gwdg.de:imprs_mps/python-dojo.git
# go to the newly checked out repo:
$ cd python-dojo
```

### Setup
To install all dependencies needed for development install `pipenv`
```bash
$ pip install pipenv
$ pipenv install
```

To check if you are set up run

```bash
$ pipenv shell
(python-dojo)$ pytest
```

If this reports all tests passed, installation is complete. You can now start and play with this repo

## Contributing
If you want to contribute, you need to ask for access to this 
repository. 

### Bug Reports & Feature Request
Bug reports and pull/merge requests are welcome on GWDG GitLab.

### Examples & Resources
Any examples, resources etc. are highly welcome. Please add them to the wiki 
or add new content in the repo and create a merge request. Please note that all 
your code will be provided to others under MIT license (see below).

### Add dependencies
As we are using `pipenv` to manage dependencies it is strictly necessary to
(un)install dependencies via this tool to ensure they are added to the `Pipfile`
and `Pipfile.lock`. Usage
```bash
$ pipenv install <package>
```

### Code of Conduct
Everyone interacting in the Tashi project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the code of conduct.
This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere 
to the [code of conduct](CODE_OF_CONDUCT.md).

## Contact
This code is developed and maintained at the Max Planck Institute for Solar System Research (MPS)

### Maintainer(s) 
- Johannes Hölken ([hoelken@mps.mpg.de](mailto:hoelken@mps.mpg.de))

## License
MIT. For details see [LICENsE](LICENSE) file.
