#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Minimal mocking example

In unit testing you want to mock out (=replace with less complex objects) anything that
either has nothing to do with the method under test directly (such as 3rd party libraries)
or things that take too long (I/O or networking, waiting code, etc.)

pytest has a simple mocking interface for that.
The mocker object is passed to the method as a fixture, and can be used without initialisation.
There is also the possibility to create the mocker explicitly, but as long as you just want to replace method calls,
the fixture approach is good enough and saves a lot of code.
See [pytest mocking API](https://pypi.org/project/pytest-mock/) for details.

A short introduction to mocking can be found in this blog post: https://changhsinlee.com/pytest-mock/

@author: hoelken
"""

from src.dojo import DeepThought


def test_define_return_value(mocker) -> None:
    """
    Minimal example on how to bypass an actual method call and return a defined value right away

    In this case we do not want to wait for seven and a half million years ;)
    """
    # We use the internal python lookup path to the method as an identifier and define the value we want to
    # have as a result via keyword argument.
    # Note that this is totally unrelated to whatever the actual return value would be
    mocker.patch('src.dojo.testing_examples.computers.DeepThought.compute_answer', return_value=12)
    assert DeepThought.compute_answer() == 12


def test_use_replacement_method(mocker) -> None:
    """
    Minimal example on how to bypass an actual method call and return a defined value right away

    In this case we do not want to wait for seven and a half million years ;)
    """
    # We use the path to the method as an identifier and define the new method to call instead.
    # Also here the return value is taken from the replacement method.
    mocker.patch('src.dojo.testing_examples.computers.DeepThought.compute_answer', new=__replacement_method)
    assert DeepThought.compute_answer() == 42


def __replacement_method() -> int:
    """
    Replacement method should mimic the signature of the real method
    :return: the answer
    """
    return 42
