"""
If you need to configure a lot of return values conditioned on the arguments a mocked method was called with
the usual way can become quite convoluted:

    def side_effect_func(value):
        if value == 1:
            return 'foo'
        #...

    m = mocker.patch('foo.bar', new=side_effect_func)
    m(1)

Placing all the conditionals in the method isn't easy to understand. Readers of your test would need to jump to the
replacement method to see the full setup. Instead, we would like to read something like this

    ConditionalMock(mocker, 'foo.bar').expect(1, return_value='foo')

In the test definition. The below example presents such a wrapper.

@author: hoelken@mps.mpg.de
"""
import time


class ConditionalMock:

    def __init__(self, mocker, path):
        self.mock = mocker.patch(path, new=self._replacement)
        self._side_effects = {}
        self._default = None
        self._raise_if_not_matched = True

    def expect(self, condition, return_value):
        condition = tuple(condition)
        self._side_effects[condition] = return_value
        return self

    def default_return(self, default):
        self._raise_if_not_matched = False
        self._default = default
        return self

    def _replacement(self, *args):
        if args in self._side_effects:
            return self._side_effects[args]
        if self._raise_if_not_matched:
            raise AssertionError(f'Arguments {args} not expected')
        return self._default


def test_conditional_mocker(mocker) -> None:
    ConditionalMock(mocker, 'time.strftime').expect('a', return_value='b').expect((1, 2), return_value='c')
    assert time.strftime('a') == 'b'
    assert time.strftime(1, 2) == 'c'
