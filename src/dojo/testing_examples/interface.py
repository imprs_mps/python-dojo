#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A simple Command Line Interface (CLI) implementation based on
python buildins input() and print().

@author: hoelken
"""


def write_to_system_out(message: str = 'Hello World') -> None:
    """Write the given message to the standard out"""
    print(message)


def get_user_input(message: str = 'Please type your message') -> str:
    """Requests user input and returns it as string"""
    return str(input(message))
