#!/bin/bash

CODE=0

# Perform style check in src folder
pycodestyle src/ --max-line-length=120
RESULT=$?
CODE=$((CODE + RESULT))

# Perform style check in tests folder
# Ignores E402: "E402 module level import not at top of file"
pycodestyle tests/ --max-line-length=120 --ignore=E402
RESULT=$?
CODE=$((CODE + RESULT))

exit $CODE