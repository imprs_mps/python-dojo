"""
## Decorator

A Decorator is an object that mimics the (relevant) interface of the object it decorates and adds behaviour.

### What is it good for? / Advantages
With a decorator one can add / alter behaviour of an object in a simple way without using
inheritance or introducing other dependencies.
One can decorate one or multiple methods of the underlying object simultaneously.
Decorators are chainable!

### Disadvantages
If the interface is only partially mimicked the scope of the original object is narrowed.

### Usage
See `notebooks/decorator_design_pattern.ipynb` for usage examples
and `/tests/design_patterns` folder for tests.


### Further Read
Wikipedia: https://en.wikipedia.org/wiki/Decorator_pattern

### Author
hoelken@mps.mpg.de
"""

import re


class Stringifyer:

    def stringify(self, obj: object):
        """
        Returns a string representation of the given object
        """
        return str(obj)


class Filter:

    def __init__(self, stringifier, pattern: str, replacement: str = ''):
        self.stringifier = stringifier
        self.filter_pattern = pattern
        self.replacement = replacement

    def stringify(self, obj: object):
        """
        Returns a string representation of the given object but replaces parts matching the given pattern with
        the given replacement.
        """
        # first call the method to decorate
        text = self.stringifier.stringify(obj)
        # now add behaviour
        return re.sub(self.filter_pattern, self.replacement, text)
