#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Example for working mit temporary files and directories

In some integration tests you may want to check dedicated I/O methods.
Sometimes your code has to write. Dangling testfiles are a pitty as they clutter up your repository/working directory
and might have unexpeted side effects weather they exist or not.

One way around this is to use setup and teardown methods (see `setup_teardown_example_test.py`).
However, it is even nicer to not have to think about the cleanup yourself. Python has a neat `tempfile`
package that provides you with a contextmanager to create temporary files and directories.

@author: hoelken
"""
import os
import tempfile
import pathlib


def test_write_to_temp_file():
    with tempfile.TemporaryFile() as temp_file:
        # We get an opened temp-file and can directly
        # write data to it:
        temp_file.write(b'Hello world!')

        # Reset pointer to beginning of file
        temp_file.seek(0)
        # Now we can read
        assert 'Hello world!', temp_file.read()
    # The temp file is deleted once we leave the block.


def test_work_with_temp_dir():
    with tempfile.TemporaryDirectory() as temp_dir:
        testfile = os.path.join(temp_dir, 'test.txt')
        assert not os.path.exists(testfile)
        pathlib.Path(testfile).touch()
        assert os.path.exists(testfile)
    # The temp directory and all contents are deleted once we leave the block.
    assert not os.path.exists(testfile)
