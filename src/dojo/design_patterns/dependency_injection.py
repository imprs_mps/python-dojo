"""
## Dependency Injection

Dependency injection (DI) is a technique in which an object (client) receives it dependencies (services) from the
caller (injector), rather than creating them itself. Instead of the client specifying which service it will use,
the injector tells the client what service to use.

### What is it good for? / Advantages
Creating objects directly within the class commits the class to particular implementations.
This makes it difficult to change the instantiation at runtime. Dependency injection solves the following problems:

- How can a class be flexible on the creation of the objects it depends on?
- How can the way objects are created be specified in separate configuration files?
- How can an application support different configurations?

If your code allows for DI, it becomes much easier to test, as jou can pass mock objects directly rather than
patching the constructor.

### Disadvantages
- Creates clients that demand configuration details, which can be onerous when obvious defaults are available.
- Typically requires more upfront development effort.
- Forces complexity out of classes and into the links between classes which might be harder to manage.

### Usage
See usage examples in the `/tests/design_patterns` folder.

### Further Read
Wikipedia: https://en.wikipedia.org/wiki/Dependency_injection

### Author
hoelken@mps.mpg.de
"""


class DependencyInjectionError(RuntimeError):
    """Custom error class to highlight we have given a wrong dependency"""
    pass


class ConstructorInjection:
    """
    Example implementation of a client that requires its dependencies on construction.

    This can be a first step towards making the client immutable and therefore thread safe.
    However, on its own, this pattern lacks the flexibility to have its dependencies changed after creation.
    """

    def __init__(self, dependency):
        # It is a good practice to perform a check on the injected dependency.
        # Here we do a simple NULL check.
        if dependency is None:
            raise DependencyInjectionError('Dependency must not be None.')
        self.dependency = dependency

    def call_dependency(self) -> str:
        """Minimal example to call service class"""
        return self.dependency.__class__.__name__


class SetterInjection:
    """
    Example implementation of a client that allows to inject dependencies at any time.
    It also allows to create the object before all dependencies exit and can therefore help to decouple the
    setup process.

    To ensure we are always in a proper state and to increase flexibility in the usage we will
    combine this with the `Lazy Initialisation Pattern`.
    """

    def __init__(self):
        self.dependency = None

    def set_dependency(self, dependency) -> None:
        """Set and change dependency at runtime"""
        # It is a good practice to perform a check on the injected dependency.
        # Here we do a simple NULL check.
        if dependency is None:
            raise DependencyInjectionError('Dependency must not be None.')
        self.dependency = dependency

    def call_dependency(self) -> str:
        """Minimal example to call service class"""
        # Note that we use the getter method and not the dependency directly!
        return self.__dependency().__class__.__name__

    def __dependency(self):
        """Lazy Getter: If the dependency exists already use it. Otherwise, construct it first."""
        if self.dependency is None:
            # For simplicity, we use a build in type. In the real world your dependency can be anything.
            self.dependency = list()
        return self.dependency
