#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Test class unit test example

In the setup we will create a test class. This will allow us to use
teardown and setup mechanisms and to structure ore tests even more.

Also, we have more sophisticated assertion methods at hand.

Methods starting with `test_` will be recognised and executed
by `pytest` automatically.

Find out more on TestCase support at https://docs.pytest.org/en/6.2.x/unittest.html

@author: hoelken
"""

import unittest


class MyTest(unittest.TestCase):
    """
    Minimal test class demonstration
    """

    def test_upper(self):
        # Example for equality check
        self.assertEqual('foo'.upper(), 'FOO')
        self.assertEqual(MyTest.__test_sting().split(), ['hello', 'world'])

    def test_isupper(self):
        # Simple example for assertTrue and assertFalse
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_for_exception(self):
        # Check that split fails when the separator is not a string
        # Same as we have done in the minimal exception example
        with self.assertRaises(TypeError) as ctx:
            MyTest.__test_sting().split(2)
        # check for (parts) of the exception message via regexp assertion:
        self.assertRegex(str(ctx.exception), r'^.*, not int$')

    @staticmethod
    def __test_sting() -> str:
        return 'hello world'
